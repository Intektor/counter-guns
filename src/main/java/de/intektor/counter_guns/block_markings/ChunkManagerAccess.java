package de.intektor.counter_guns.block_markings;

import net.minecraft.world.server.ChunkHolder;
import net.minecraft.world.server.ChunkManager;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ChunkManagerAccess {

    private static MethodHandle getLoadedChunksMethodMh;

    static {
        final Method getLoadedChunksMethod = ObfuscationReflectionHelper.findMethod(ChunkManager.class, "func_223491_f");
        getLoadedChunksMethod.setAccessible(true);

        try {
            getLoadedChunksMethodMh = MethodHandles.publicLookup().unreflect(getLoadedChunksMethod);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static Iterable<ChunkHolder> getLoadedChunks(ChunkManager chunkManager) throws Throwable {
        //noinspection unchecked
        return (Iterable<ChunkHolder>) getLoadedChunksMethodMh.invokeExact(chunkManager);
    }
}
