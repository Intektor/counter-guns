package de.intektor.counter_guns.client;

import it.unimi.dsi.fastutil.objects.ObjectList;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.chunk.ChunkRenderDispatcher;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WorldRendererAccess {

    private static final MethodHandle renderInfosMh;
    private static final MethodHandle renderChunkMh;

    static {
        try {
            Class localRenderInformationContainerClass =
                    Arrays.stream(WorldRenderer.class.getDeclaredClasses())
                            .filter(clazz -> clazz.getName().equals("net.minecraft.client.renderer.WorldRenderer$LocalRenderInformationContainer"))
                    .findFirst()
                    .get();
            Field renderInfosField = ObfuscationReflectionHelper.findField(WorldRenderer.class, "field_72755_R");
            Field renderChunkField = ObfuscationReflectionHelper.findField(localRenderInformationContainerClass, "field_178036_a");

            renderInfosField.setAccessible(true);

            renderInfosMh = MethodHandles.publicLookup().unreflectGetter(renderInfosField);
            renderChunkMh = MethodHandles.publicLookup().unreflectGetter(renderChunkField);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static Stream<ChunkRenderDispatcher.ChunkRender> getRenderInfos(WorldRenderer renderer) throws Throwable {
        ObjectList<?> renderInfos = (ObjectList<?>) renderInfosMh.invokeExact(renderer);
        return renderInfos.stream().map((renderInfo) -> {
            try {
                //TODO: invoke exact working?
                return (ChunkRenderDispatcher.ChunkRender) renderChunkMh.invoke(renderInfo);
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        });
    }
}
