package de.intektor.counter_guns.entity

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.GrenadeType
import de.intektor.counter_guns.block_markings.BlockMarkings
import de.intektor.counter_guns.net.to_client.FlashedMessage
import de.intektor.counter_guns.util.cubeBoundingBox
import de.intektor.counter_guns.util.minus
import de.intektor.counter_guns.util.plus
import de.intektor.counter_guns.util.times
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.IRendersAsItem
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.entity.projectile.ThrowableEntity
import net.minecraft.item.ItemStack
import net.minecraft.nbt.CompoundNBT
import net.minecraft.network.IPacket
import net.minecraft.network.PacketBuffer
import net.minecraft.network.datasync.DataSerializers
import net.minecraft.network.datasync.EntityDataManager
import net.minecraft.util.Direction
import net.minecraft.util.math.*
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.Explosion
import net.minecraft.world.World
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData
import net.minecraftforge.fml.network.NetworkHooks
import net.minecraftforge.fml.network.PacketDistributor
import kotlin.math.acos

/**
 * Entity class of all grenades
 * @author Intektor
 */
internal class GrenadeEntity(
        type: EntityType<out GrenadeEntity>,
        world: World,
        private var grenadeType: GrenadeType? = null) : ThrowableEntity(type, world), IEntityAdditionalSpawnData, IRendersAsItem {

    var shouldSpreadSmoke: Boolean
        set(value) = dataManager.set(SHOULD_SPREAD_SMOKE, value)
        get() = dataManager.get(SHOULD_SPREAD_SMOKE)

    private var hasSpawnedSmoke = false

    /**
     * the ticks this entity has existed when it spawned the smoke.
     * only set on server
     */
    private var ticksWhenSmokeSpread = 0

    constructor(type: EntityType<out GrenadeEntity>, world: World) : this(type, world, null)

    companion object {
        private val SHOULD_SPREAD_SMOKE = EntityDataManager.createKey(GrenadeEntity::class.java, DataSerializers.BOOLEAN)
    }

    override fun createSpawnPacket(): IPacket<*> = NetworkHooks.getEntitySpawningPacket(this)

    override fun registerData() {
        dataManager.register(SHOULD_SPREAD_SMOKE, false)
    }

    override fun tick() {
        super.tick()

        when (grenadeType) {
            GrenadeType.SMOKE -> if (shouldSpreadSmoke) {
                if (ticksExisted - ticksWhenSmokeSpread > 40 && !world.isRemote) {
                    setDead()
                }
                if (world.isRemote && !hasSpawnedSmoke) {
                    spawnSmoke()
                    hasSpawnedSmoke = true
                    setDead()
                }
            }
            GrenadeType.INCENDIARY -> {

            }
            GrenadeType.HIGH_EXPLOSIVE, GrenadeType.FLASH -> if (ticksExisted >= 40) {
                onDetonation()
                setDead()
            }
        }
    }

    /**
     * Called on the server after the lifespan of a grenade has expired and is now supposed to explode
     */
    private fun onDetonation() {
        when (grenadeType) {
            GrenadeType.SMOKE -> {
                spawnSmoke()
            }
            GrenadeType.INCENDIARY -> {

            }
            GrenadeType.HIGH_EXPLOSIVE -> {
                spawnHeDetonation()
            }
            GrenadeType.FLASH -> performFlash()
        }
    }

    private fun spawnSmoke() {
        if (world.isRemote) {
            val radius = 3
            val allInBox = BlockPos.getAllInBox(
                    BlockPos(posX - radius, posY - 1, posZ - radius),
                    BlockPos(posX + radius, posY + 2 * radius - 1, posZ + radius)
            )

            val center = BlockPos(posX, posY + radius - 1, posZ)

            allInBox.forEach { pos ->
                if (pos.distanceSq(center) <= 9) {
                    (0 until 1).forEach { _ ->
                        world.addParticle(
                                CounterGuns.hugeSmokeParticle.get(),
                                pos.x.toDouble(),
                                pos.y.toDouble(),
                                pos.z.toDouble(),
                                0.0,
                                0.0,
                                0.0)
                    }
                }
            }
        }
    }

    private fun spawnHeDetonation() {
        if (!world.isRemote) {
            val explodeMode = if (CounterGuns.config.dealHeWorldDamage) Explosion.Mode.DESTROY else Explosion.Mode.NONE
            world.createExplosion(this, posX, posY, posZ, 4f, false, explodeMode)

            val entitiesInExplosion = getAllEntitiesInCube(4.0, 4f, LivingEntity::class.java)
            entitiesInExplosion.forEach { entity ->
                val hitVec = entity.boundingBox.center - boundingBox.center
                BlockMarkings.splatterBlood(world as ServerWorld, entity.boundingBox.center, hitVec, 20)
            }
        }
    }

    private fun performFlash() {
        if (!world.isRemote) {
            val allPlayersInRange = getAllEntitiesInCube(120.0, null, PlayerEntity::class.java)

            allPlayersInRange.forEach { player ->
                val playerEyeHeight = player.getEyeHeight(player.pose).toDouble()
                val playerFaceCenter = player.positionVec + Vector3d(0.0, playerEyeHeight, 0.0)

                val grenadeCenter = getBoundingBox(pose).center

                val collideWithBlock = world.rayTraceBlocks(
                        RayTraceContext(grenadeCenter, playerFaceCenter, RayTraceContext.BlockMode.VISUAL, RayTraceContext.FluidMode.NONE, null)
                )

                if (!world.getBlockState(collideWithBlock.pos).isSolid) {
                    val lightVec = playerFaceCenter - grenadeCenter
                    val playerLookVec = player.lookVec

                    val angle = acos(lightVec.dotProduct(playerLookVec) / (lightVec.length() * playerLookVec.length()))
                    val intensity = when (Math.toDegrees(angle)) {
                        in 150.0..180.0 -> 1.0
                        in 120.0..150.0 -> 0.6
                        in 90.0..120.0 -> 0.3
                        else -> 0.0
                    }.toFloat()

                    CounterGuns.networkChannel.send(PacketDistributor.PLAYER.with { player as ServerPlayerEntity }, FlashedMessage(intensity))
                }
            }
        }
    }

    private fun <T : Entity> getAllEntitiesInCube(sideLength: Double, withinDistance: Float?, entityClass: Class<T>): List<T> {
        return world.getEntitiesWithinAABB(entityClass, cubeBoundingBox(positionVec, sideLength)) {
            withinDistance == null || positionVec.squareDistanceTo(it.positionVec) < withinDistance * withinDistance
        }
    }

    override fun onImpact(result: RayTraceResult) {
        if (result is BlockRayTraceResult) {
            if (result.face != null && world.getBlockState(result.pos).isSolid) {
                val mulVec = when (result.face) {
                    Direction.DOWN, Direction.UP -> Vector3d(1.0, -1.0, 1.0)
                    Direction.NORTH, Direction.SOUTH -> Vector3d(1.0, 1.0, -1.0)
                    Direction.WEST, Direction.EAST -> Vector3d(-1.0, 1.0, 1.0)
                }

                motion *= mulVec * 0.5
            }
            setPosition(result.hitVec.x, result.hitVec.y, result.hitVec.z)
        } else if (result is EntityRayTraceResult) {
            motion *= 0.3
        }
        if (motion.length() < 0.1 && !world.isRemote) {
            motion *= Vector3d(1.0, 0.0, 1.0)
            shouldSpreadSmoke = true
            ticksWhenSmokeSpread = ticksExisted
        }
    }

    override fun writeAdditional(compound: CompoundNBT) {
        super.writeAdditional(compound)
        compound.putInt("grenadeType", grenadeType?.ordinal ?: 0)
    }

    override fun readAdditional(compound: CompoundNBT) {
        super.readAdditional(compound)
        grenadeType = GrenadeType.values()[compound.getInt("grenadeType")]
    }

    override fun writeSpawnData(buffer: PacketBuffer) {
        buffer.writeInt(grenadeType?.ordinal ?: 0)
    }

    override fun readSpawnData(additionalData: PacketBuffer) {
        grenadeType = GrenadeType.values()[additionalData.readInt()]
    }

    override fun getItem(): ItemStack {
        val grenadeType = grenadeType
        check(grenadeType != null) { "Grenade type must not be null for entity=$this" }
        return ItemStack(grenadeType.item().get())
    }
}