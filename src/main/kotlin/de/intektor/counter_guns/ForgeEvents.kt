package de.intektor.counter_guns

import de.intektor.counter_guns.block_markings.BlockMarkingChunkCap
import de.intektor.counter_guns.block_markings.BlockMarkings
import de.intektor.counter_guns.block_markings.ChunkManagerAccess
import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.get
import de.intektor.counter_guns.data.GunPropertyManager
import de.intektor.counter_guns.net.to_client.InitialChunkBlockMarkingsMessage
import net.minecraft.world.server.ChunkManager
import net.minecraft.world.server.ServerChunkProvider
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.client.event.ClientPlayerNetworkEvent
import net.minecraftforge.event.AddReloadListenerEvent
import net.minecraftforge.event.ForgeEventFactory
import net.minecraftforge.event.TickEvent
import net.minecraftforge.event.world.*
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.network.NetworkEvent
import net.minecraftforge.fml.network.PacketDistributor

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
object ForgeEvents {

    private val toExecute = ArrayDeque<() -> Unit>()

    @JvmStatic
    @SubscribeEvent
    fun playerTick(event: TickEvent.PlayerTickEvent) {
        event.player[CounterGunsPlayerCap.cap]?.onPlayerTick()
    }

    @JvmStatic
    @SubscribeEvent
    fun serverTick(event: TickEvent.ServerTickEvent) {
        if (event.phase == TickEvent.Phase.END) {
            toExecute.forEach { it.invoke() }
            toExecute.clear()
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun chunkWatchEvent(event: ChunkWatchEvent.Watch) {
        toExecute += {
            val chunk = event.world.getChunk(event.pos.x, event.pos.z)
            val blockMarkingsCap = chunk[BlockMarkingChunkCap.cap]
            check(blockMarkingsCap != null) { "chunk=$chunk should have a capability" }
            if (blockMarkingsCap.chunkBlockMarkings.isNotEmpty()) {
                val msg = InitialChunkBlockMarkingsMessage(event.pos.x, event.pos.z, HashMap(blockMarkingsCap.chunkBlockMarkings))
                CounterGuns.networkChannel.send(PacketDistributor.PLAYER.with { event.player }, msg)
            }
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun blockDestroyed(event: BlockEvent.NeighborNotifyEvent) {
        if (!event.world.getBlockState(event.pos).isSolid) {
            BlockMarkings.removeBlockMarkingsFromBlock(event.world as ServerWorld, event.pos)
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun worldTick(event: TickEvent.WorldTickEvent) {
        val world = event.world
        val chunkProvider = world.chunkProvider
        if (world is ServerWorld && chunkProvider is ServerChunkProvider) {
            ChunkManagerAccess.getLoadedChunks(chunkProvider.chunkManager).forEach { chunkHolder ->
                val chunk = chunkHolder.chunkIfComplete ?: return@forEach

                val cap = chunk[BlockMarkingChunkCap.cap] ?: return@forEach
                BlockMarkings.tickChunkWithMarkings(chunk, cap)
            }
        }
    }
}