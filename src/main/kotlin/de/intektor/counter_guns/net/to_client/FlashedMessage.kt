package de.intektor.counter_guns.net.to_client

import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.get
import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor

class FlashedMessage(val flashAmount: Float) {
    companion object {
        fun read(buf: ByteBuf): FlashedMessage {
            return FlashedMessage(buf.readFloat())
        }
    }

    fun write(buf: ByteBuf) {
        buf.writeFloat(flashAmount)
    }

    fun handle() {
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable {
                val mc = Minecraft.getInstance()
                val player = mc.player ?: return@SafeRunnable

                player[CounterGunsPlayerCap.cap]?.flashPlayer(flashAmount)
            }
        }
    }
}