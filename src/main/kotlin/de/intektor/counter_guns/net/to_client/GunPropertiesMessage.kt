package de.intektor.counter_guns.net.to_client

import de.intektor.counter_guns.data.GunProperties
import de.intektor.counter_guns.data.GunProperties.Companion.readGunProperties
import de.intektor.counter_guns.data.GunProperties.Companion.writeGunProperties
import de.intektor.counter_guns.data.GunPropertyManager
import de.intektor.counter_guns.net.readMap
import de.intektor.counter_guns.net.writeMap
import net.minecraft.network.PacketBuffer
import net.minecraft.util.ResourceLocation
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor

class GunPropertiesMessage(
    private val properties: Map<ResourceLocation, GunProperties>
) {
    constructor(buf: PacketBuffer) : this(buf.readMap({ readResourceLocation() }, { readGunProperties() }))

    fun write(buf: PacketBuffer) {
        buf.writeMap(properties, { writeResourceLocation(it) }, { writeGunProperties(it) })
    }

    fun execute() {
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable {
                GunPropertyManager.clientSideInstance.injectServerValues(properties)
                println("received gun properties $properties")
            }
        }
    }

}