package de.intektor.counter_guns.net.to_client

import de.intektor.counter_guns.block_markings.BlockMarking
import de.intektor.counter_guns.block_markings.ClientMarkingUtil
import de.intektor.counter_guns.net.readMap
import de.intektor.counter_guns.net.readUUID
import de.intektor.counter_guns.net.writeMap
import de.intektor.counter_guns.net.writeUUID
import net.minecraft.network.PacketBuffer
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor
import java.util.*

internal class InitialChunkBlockMarkingsMessage(
        private val chunkX: Int,
        private val chunkZ: Int,
        private val chunkMarkings: Map<UUID, BlockMarking>) {

    companion object {
        fun read(buf: PacketBuffer): InitialChunkBlockMarkingsMessage {
            val chunkX = buf.readInt()
            val chunkZ = buf.readInt()
            val chunkMarkings: Map<UUID, BlockMarking> = buf.readMap({
                readUUID()
            }, {
                BlockMarking.read(buf)
            })

            return InitialChunkBlockMarkingsMessage(chunkX, chunkZ, chunkMarkings)
        }
    }

    fun write(buf: PacketBuffer) {
        buf.writeInt(chunkX)
        buf.writeInt(chunkZ)
        buf.writeMap(chunkMarkings, {
            writeUUID(it)
        }, {
            it.write(buf)
        })
    }

    fun execute() {
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable {
                ClientMarkingUtil.getChunkCapability(chunkX, chunkZ).addAllMarkings(chunkMarkings)
            }
        }
    }
}