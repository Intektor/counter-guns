package de.intektor.counter_guns.net.to_client

import de.intektor.counter_guns.block_markings.BlockMarking
import de.intektor.counter_guns.block_markings.ClientMarkingUtil
import de.intektor.counter_guns.net.readList
import de.intektor.counter_guns.net.readUUID
import de.intektor.counter_guns.net.writeCollection
import de.intektor.counter_guns.net.writeUUID
import net.minecraft.network.PacketBuffer
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor
import java.util.*

internal class UpdateChunkBlockMarkingsMessage(
        private val chunkX: Int,
        private val chunkZ: Int,
        private val removed: List<UUID>,
        private val added: List<Pair<UUID, BlockMarking>>
) {
    companion object {
        fun read(buf: PacketBuffer): UpdateChunkBlockMarkingsMessage {
            val chunkX = buf.readInt()
            val chunkZ = buf.readInt()

            val removed = buf.readList { readUUID() }
            val added = buf.readList {
                val uuid = readUUID()
                val marking = BlockMarking.read(buf)
                Pair(uuid, marking)
            }
            return UpdateChunkBlockMarkingsMessage(chunkX, chunkZ, removed, added)
        }
    }

    fun write(buf: PacketBuffer) {
        buf.writeInt(chunkX)
        buf.writeInt(chunkZ)
        buf.writeCollection(removed) { buf.writeUUID(it) }
        buf.writeCollection(added) {
            buf.writeUUID(it.first)
            it.second.write(buf)
        }
    }

    fun execute() {
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable {
                val chunkCapability = ClientMarkingUtil.getChunkCapability(chunkX, chunkZ)
                chunkCapability.removeMarkingsWithId(removed)
                chunkCapability.addAllMarkings(added)
            }
        }
    }
}