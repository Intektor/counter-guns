package de.intektor.counter_guns.net.to_server

import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.get
import io.netty.buffer.ByteBuf
import net.minecraftforge.fml.network.NetworkEvent

class PlayerLeftClickMessage(private val isLeftClicking: Boolean) {

    companion object {
        fun read(buf: ByteBuf): PlayerLeftClickMessage {
            return PlayerLeftClickMessage(buf.readBoolean())
        }
    }

    fun write(buf: ByteBuf) {
        buf.writeBoolean(isLeftClicking)
    }

    fun handle(ctx: NetworkEvent.Context) {
        val sender = ctx.sender ?: return
        val capability = sender[CounterGunsPlayerCap.cap] ?: return
        capability.isLeftClicking = isLeftClicking
    }
}