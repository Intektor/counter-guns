package de.intektor.counter_guns.net.to_server

import de.intektor.counter_guns.GunShots
import net.minecraft.network.PacketBuffer
import net.minecraft.util.Direction
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.fml.network.NetworkEvent

internal class ShotFiredMessage(val partialTicks: Float,
                                val rotationPitch: Float,
                                val rotationYaw: Float,
                                val worldTick: Long,
                                val shotData: ShotData) {

    companion object {
        fun read(buf: PacketBuffer): ShotFiredMessage {
            val partialTicks = buf.readFloat()
            val rotationPitch = buf.readFloat()
            val rotationYaw = buf.readFloat()
            val worldTick = buf.readLong()

            val shotData = when (buf.readByte().toInt()) {
                0 -> BlockShotData.read(buf)
                1 -> EntityShotData.read(buf)
                2 -> EmptyShotData()
                else -> EmptyShotData()
            }

            return ShotFiredMessage(partialTicks, rotationPitch, rotationYaw, worldTick, shotData)
        }
    }

    fun write(buf: PacketBuffer) {
        buf.writeFloat(partialTicks)
        buf.writeFloat(rotationPitch)
        buf.writeFloat(rotationYaw)
        buf.writeLong(worldTick)
        buf.writeByte(shotData.id)
        shotData.write(buf)
    }

    fun execute(ctx: NetworkEvent.Context) {
        val sender = ctx.sender ?: return
        GunShots.processShotFromClient(sender, sender.world as ServerWorld, this)
    }

    abstract class ShotData {
        abstract val id: Int

        abstract fun write(buf: PacketBuffer)
    }

    class BlockShotData(val hitPos: Vector3d, val side: Direction, val blockPos: BlockPos) : ShotData() {

        companion object {
            fun read(buf: PacketBuffer): BlockShotData {
                val hitPos = Vector3d(buf.readDouble(), buf.readDouble(), buf.readDouble())
                val side = Direction.values()[buf.readByte().toInt()]
                val blockPos = buf.readBlockPos()

                return BlockShotData(hitPos, side, blockPos)
            }
        }

        override val id: Int
            get() = 0

        override fun write(buf: PacketBuffer) {
            buf.writeDouble(hitPos.x)
            buf.writeDouble(hitPos.y)
            buf.writeDouble(hitPos.z)

            buf.writeByte(side.ordinal)
            buf.writeBlockPos(blockPos)
        }
    }

    class EntityShotData(val hitPos: Vector3d, val entityId: Int, val headshot: Boolean) : ShotData() {

        companion object {
            fun read(buf: PacketBuffer): EntityShotData {
                val hitPos = Vector3d(buf.readDouble(), buf.readDouble(), buf.readDouble())
                val entityId = buf.readInt()
                val headshot = buf.readBoolean()

                return EntityShotData(hitPos, entityId, headshot)
            }
        }

        override val id: Int
            get() = 1

        override fun write(buf: PacketBuffer) {
            buf.writeDouble(hitPos.x)
            buf.writeDouble(hitPos.y)
            buf.writeDouble(hitPos.z)

            buf.writeInt(entityId)
            buf.writeBoolean(headshot)
        }
    }

    class EmptyShotData() : ShotData() {
        override val id: Int
            get() = 2

        override fun write(buf: PacketBuffer) {

        }
    }
}