package de.intektor.counter_guns.net

import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableMap
import net.minecraft.network.PacketBuffer
import java.util.*

inline fun <T> PacketBuffer.writeCollection(collection: Collection<T>, writer: PacketBuffer.(item: T) -> Unit) {
    writeVarInt(collection.size)
    for (element in collection) {
        writer(element)
    }
}

inline fun <K, V> PacketBuffer.writeMap(map: Map<K, V>, kWriter: PacketBuffer.(K) -> Unit, vWriter: PacketBuffer.(V) -> Unit) {
    writeVarInt(map.size)
    for((k, v) in map) {
        kWriter(k)
        vWriter(v)
    }
}

inline fun <K, V> PacketBuffer.readMap(kReader: PacketBuffer.() -> K, vReader: PacketBuffer.() -> V): Map<K, V> {
    return readMap({ ImmutableMap.builder() }, ImmutableMap.Builder<K, V>::put, kReader, vReader, ImmutableMap.Builder<K, V>::build)
}

inline fun <K, V, B, M : Map<K, V>> PacketBuffer.readMap(
    builderFactory: (count: Int) -> B, adder: B.(K, V) -> Unit, kReader: PacketBuffer.() -> K, vReader: PacketBuffer.() -> V,
    creator: B.() -> M
): M {
    val count = readVarInt()
    val builder = builderFactory(count)
    for (idx in 0 until count) {
        builder.adder(kReader(), vReader())
    }
    return builder.creator()
}

inline fun <T> PacketBuffer.readList(reader: PacketBuffer.() -> T): List<T> {
    return readCollection(
        {  ImmutableList.builder() }, ImmutableList.Builder<T>::add, reader, ImmutableList.Builder<T>::build
    )
}

inline fun <T, R : MutableCollection<T>> PacketBuffer.readCollection(factory: (count: Int) -> R, reader: PacketBuffer.() -> T): R {
    return readCollection(factory, MutableCollection<T>::add, reader, { this })
}

inline fun <T, R : Collection<T>, B> PacketBuffer.readCollection(builderFactory: (count: Int) -> B, adder: B.(T) -> Unit, reader: PacketBuffer.() -> T, creator: B.() -> R): R {
    val count = readVarInt()
    val builder = builderFactory(count)
    for (idx in 0 until count) {
        builder.adder(reader())
    }
    return builder.creator()
}

fun PacketBuffer.writeUUID(uuid: UUID) {
    writeLong(uuid.mostSignificantBits)
    writeLong(uuid.leastSignificantBits)
}

fun PacketBuffer.readUUID(): UUID = UUID(readLong(), readLong())