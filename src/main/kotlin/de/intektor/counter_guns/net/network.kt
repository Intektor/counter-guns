package de.intektor.counter_guns.net

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.net.to_client.FlashedMessage
import de.intektor.counter_guns.net.to_client.GunPropertiesMessage
import de.intektor.counter_guns.net.to_client.InitialChunkBlockMarkingsMessage
import de.intektor.counter_guns.net.to_client.UpdateChunkBlockMarkingsMessage
import de.intektor.counter_guns.net.to_server.PlayerLeftClickMessage
import de.intektor.counter_guns.net.to_server.ShotFiredMessage
import net.minecraft.network.PacketBuffer
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.network.NetworkDirection
import net.minecraftforge.fml.network.NetworkEvent
import net.minecraftforge.fml.network.NetworkRegistry
import net.minecraftforge.fml.network.simple.SimpleChannel
import java.util.*

val networkChannelId = ResourceLocation(CounterGuns.ID, "network")
private const val networkVersion = "1"

fun createNetworkChannel(): SimpleChannel {
    @Suppress("INACCESSIBLE_TYPE")
    return NetworkRegistry.newSimpleChannel(networkChannelId, { networkVersion }, networkVersion::equals, networkVersion::equals).apply {
        registerMessage(
                0,
                PlayerLeftClickMessage::write,
                PlayerLeftClickMessage.Companion::read,
                PlayerLeftClickMessage::handle,
                NetworkDirection.PLAY_TO_SERVER
        )
        registerMessage(
                1,
                GunPropertiesMessage::write,
                ::GunPropertiesMessage,
                GunPropertiesMessage::execute,
                NetworkDirection.PLAY_TO_CLIENT
        )
        registerMessage(
                2,
                FlashedMessage::write,
                FlashedMessage.Companion::read,
                FlashedMessage::handle,
                NetworkDirection.PLAY_TO_CLIENT
        )
        registerMessage(
                3,
                InitialChunkBlockMarkingsMessage::write,
                InitialChunkBlockMarkingsMessage.Companion::read,
                InitialChunkBlockMarkingsMessage::execute,
                NetworkDirection.PLAY_TO_CLIENT
        )
        registerMessage(
                4,
                UpdateChunkBlockMarkingsMessage::write,
                UpdateChunkBlockMarkingsMessage.Companion::read,
                UpdateChunkBlockMarkingsMessage::execute,
                NetworkDirection.PLAY_TO_CLIENT
        )
        registerMessage(
                5,
                ShotFiredMessage::write,
                ShotFiredMessage.Companion::read,
                ShotFiredMessage::execute,
                NetworkDirection.PLAY_TO_SERVER
        )
    }
}

private inline fun <reified P : Any> SimpleChannel.registerMessage(
        index: Int,
        crossinline writer: P.(PacketBuffer) -> Unit,
        crossinline reader: (PacketBuffer) -> P,
        crossinline handler: P.() -> Unit,
        direction: NetworkDirection? = null
): Unit = registerMessage(index, writer, reader, { _ -> handler() }, direction)

private inline fun <reified P : Any> SimpleChannel.registerMessage(
        index: Int,
        crossinline writer: P.(PacketBuffer) -> Unit,
        crossinline reader: (PacketBuffer) -> P,
        crossinline handler: P.(NetworkEvent.Context) -> Unit,
        direction: NetworkDirection? = null
) {
    @Suppress("INACCESSIBLE_TYPE")
    registerMessage(index, P::class.java, { p, buf -> p.writer(buf) }, { buf -> reader(buf) }, { p, ctxSupplier ->
        val ctx = ctxSupplier.get()
        ctx.packetHandled = true
        ctx.enqueueWork { p.handler(ctx) }
    }, Optional.ofNullable(direction))
}
