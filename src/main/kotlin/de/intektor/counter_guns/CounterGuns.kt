package de.intektor.counter_guns

import de.intektor.counter_guns.block_markings.BlockMarkingChunkCap
import de.intektor.counter_guns.cap.CGLeftClickableItemCap
import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.GunItemCap
import de.intektor.counter_guns.config.CGConfig
import de.intektor.counter_guns.entity.GrenadeEntity
import de.intektor.counter_guns.item.CGItems
import de.intektor.counter_guns.net.createNetworkChannel
import net.minecraft.entity.EntityClassification
import net.minecraft.entity.EntityType
import net.minecraft.nbt.CompoundNBT
import net.minecraft.nbt.INBT
import net.minecraft.particles.BasicParticleType
import net.minecraft.particles.IParticleData
import net.minecraft.particles.ParticleType
import net.minecraft.particles.ParticleTypes
import net.minecraft.util.DamageSource
import net.minecraft.util.Direction
import net.minecraftforge.common.ForgeConfigSpec
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityManager
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.ModLoadingContext
import net.minecraftforge.fml.RegistryObject
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.config.ModConfig
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext
import net.minecraftforge.fml.network.simple.SimpleChannel
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import java.lang.UnsupportedOperationException
import kotlin.properties.Delegates

@Mod(CounterGuns.ID)
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
internal class CounterGuns {

    init {
        arrayOf(CGItems.items, entityRegistry, particleRegistry).forEach {
            it.register(FMLJavaModLoadingContext.get().modEventBus)
        }

        val config = ForgeConfigSpec.Builder().apply {
            val heGrenadeWorldDamageSpec = comment("Should HE grenades deal explosive damage to the world?")
                    .define("heGrenadeWorldDamageAllowed", true)

            config = CGConfig(heGrenadeWorldDamageSpec)
        }.build()

        ModLoadingContext.get().activeContainer.apply {
            addConfig(ModConfig(ModConfig.Type.SERVER, config, this))
        }

        networkChannel = createNetworkChannel()
    }

    companion object {

        const val ID = "counter_guns"

        var networkChannel by Delegates.notNull<SimpleChannel>()
        private val entityRegistry = DeferredRegister.create(ForgeRegistries.ENTITIES, ID)

        lateinit var config: CGConfig

        val entityGrenade: RegistryObject<EntityType<GrenadeEntity>> = entityRegistry.register("grenade") {
            EntityType.Builder.create(::GrenadeEntity, EntityClassification.MISC)
                    .size(0.1f, 0.1f)
                    .setShouldReceiveVelocityUpdates(true)
                    .setUpdateInterval(20)
                    .setTrackingRange(500)
                    .build("grenade")
        }

        private val particleRegistry = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, ID)

        val hugeSmokeParticle = particleRegistry.register("huge_smoke_particle") {
            BasicParticleType(true)
        }

        val gunDamageSource = DamageSource("gunDamage").setDamageBypassesArmor()

        @JvmStatic
        @SubscribeEvent
        fun commonInit(evt: FMLCommonSetupEvent) {
            CapabilityManager.INSTANCE.apply {
                register(CounterGunsPlayerCap::class.java, CounterGunsPlayerCap.StorageProvider()) {
                    throw UnsupportedOperationException("Do not use this capability")
                }

                register(CGLeftClickableItemCap::class.java, CGLeftClickableItemCap.StorageProvider()) {
                    throw UnsupportedOperationException("Do not use this capability")
                }

                register(BlockMarkingChunkCap::class.java, BlockMarkingChunkCap.StorageProvider()) {
                    throw UnsupportedOperationException("Do not use this capability")
                }

                register(GunItemCap::class.java, GunItemCap.StorageProvider()) {
                    throw UnsupportedOperationException("Do not use this capability")
                }
            }
        }
    }
}