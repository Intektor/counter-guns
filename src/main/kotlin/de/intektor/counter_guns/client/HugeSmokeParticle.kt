package de.intektor.counter_guns.client

import net.minecraft.client.particle.IAnimatedSprite
import net.minecraft.client.particle.IParticleFactory
import net.minecraft.client.particle.Particle
import net.minecraft.client.particle.SmokeParticle
import net.minecraft.client.world.ClientWorld
import net.minecraft.particles.BasicParticleType

class HugeSmokeParticle(
        p_i232425_1_: ClientWorld,
        p_i232425_2_: Double,
        p_i232425_4_: Double,
        p_i232425_6_: Double,
        p_i232425_8_: Double,
        p_i232425_10_: Double,
        p_i232425_12_: Double,
        p_i232425_14_: Float,
        p_i232425_15_: IAnimatedSprite) :
        SmokeParticle(
                p_i232425_1_,
                p_i232425_2_,
                p_i232425_4_,
                p_i232425_6_,
                p_i232425_8_,
                p_i232425_10_,
                p_i232425_12_,
                p_i232425_14_,
                p_i232425_15_) {

    init {
        particleScale *= 10.5f
        maxAge = 400
    }

    override fun tick() {
        prevPosX = posX
        prevPosY = posY
        prevPosZ = posZ

        if (age / maxAge >= 0.9f) {
            setAlphaF(1 - (age / maxAge - 0.9f) * 10 / 1)
        }

        if (age++ >= maxAge) {
            setExpired()
        }

        motionY = 0.0
    }

    class Factory(private val spriteSet: IAnimatedSprite) : IParticleFactory<BasicParticleType?> {
        override fun makeParticle(typeIn: BasicParticleType?, worldIn: ClientWorld, x: Double, y: Double, z: Double, xSpeed: Double, ySpeed: Double, zSpeed: Double): Particle? {
            return HugeSmokeParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed, 1.0f, spriteSet)
        }
    }
}