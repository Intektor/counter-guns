package de.intektor.counter_guns.client

import de.intektor.counter_guns.CounterGuns
import net.minecraft.client.Minecraft
import net.minecraft.client.particle.IParticleFactory
import net.minecraft.client.particle.Particle
import net.minecraft.client.particle.ParticleManager
import net.minecraft.client.renderer.entity.SpriteRenderer
import net.minecraft.client.world.ClientWorld
import net.minecraft.particles.BasicParticleType
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.client.registry.RenderingRegistry
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent

@Mod.EventBusSubscriber(value = [Dist.CLIENT], modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.MOD)
object ClientProxy {

    @JvmStatic
    @SubscribeEvent
    fun register(event: FMLClientSetupEvent) {
        RenderingRegistry.registerEntityRenderingHandler(CounterGuns.entityGrenade.get()) {
            val instance = Minecraft.getInstance()
            SpriteRenderer(instance.renderManager, instance.itemRenderer)
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun registerParticle(event: ParticleFactoryRegisterEvent) {
        Minecraft.getInstance().particles.registerFactory(CounterGuns.hugeSmokeParticle.get(), HugeSmokeParticle::Factory)
    }
}