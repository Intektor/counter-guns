package de.intektor.counter_guns.client

import com.mojang.blaze3d.matrix.MatrixStack
import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.block_markings.BlockMarking
import de.intektor.counter_guns.block_markings.BlockMarkingChunkCap
import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.GunItemCap
import de.intektor.counter_guns.cap.get
import de.intektor.counter_guns.data.GunProperties
import de.intektor.counter_guns.data.GunPropertyManager.Companion.getGunProperties
import de.intektor.counter_guns.data.GunPropertyManager.Companion.hasGunProperties
import de.intektor.counter_guns.item.GunItem
import de.intektor.counter_guns.net.to_server.PlayerLeftClickMessage
import de.intektor.counter_guns.net.to_server.ShotFiredMessage
import de.intektor.counter_guns.util.getLookVec
import de.intektor.counter_guns.util.plus
import de.intektor.counter_guns.util.rayTraceWorldAndEntities
import de.intektor.counter_guns.util.toVector
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.AbstractGui
import net.minecraft.client.world.ClientWorld
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.Direction
import net.minecraft.util.Hand
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.World
import net.minecraft.world.chunk.Chunk
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.client.event.*
import net.minecraftforge.event.TickEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import org.lwjgl.glfw.GLFW

@Mod.EventBusSubscriber(value = [Dist.CLIENT], modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
object ClientEventHandler {

    @JvmStatic
    @SubscribeEvent
    fun rawMouseInput(event: InputEvent.RawMouseEvent) {
        val minecraft = Minecraft.getInstance()
        val player = minecraft.player

        //Don't do anything if a gui is open
        if (minecraft.currentScreen != null) return

        if (player != null && event.button == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
            if (event.action == GLFW.GLFW_PRESS || event.action == GLFW.GLFW_RELEASE) {
                val isClicking = event.action == GLFW.GLFW_PRESS
                player[CounterGunsPlayerCap.cap]?.isLeftClicking = isClicking

                CounterGuns.networkChannel.sendToServer(PlayerLeftClickMessage(isClicking))
            }
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun openGui(event: GuiOpenEvent) {
        val minecraft = Minecraft.getInstance()
        val player = minecraft.player

        if (event.gui != null && player != null) {
            CounterGuns.networkChannel.sendToServer(PlayerLeftClickMessage(false))
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun renderGameOverlay(event: RenderGameOverlayEvent.Post) {
        if (Minecraft.getInstance().currentScreen == null) {
            drawFlash(event.matrixStack, event.window.width, event.window.height)
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun renderScreenEvent(event: GuiScreenEvent.DrawScreenEvent.Post) {
        val mc = Minecraft.getInstance()
        drawFlash(event.matrixStack, mc.mainWindow.width, mc.mainWindow.height)
    }

    private fun drawFlash(matrixStack: MatrixStack, width: Int, height: Int) {
        val mc = Minecraft.getInstance()
        val player = mc.player
        if (player != null && mc.renderViewEntity == player) {
            val currentFlashStrength = player[CounterGunsPlayerCap.cap]?.currentFlashAmount ?: 0.0f

            if (currentFlashStrength > 0) {
                val color = ((0xff * currentFlashStrength).toInt() shl 24) or 0xffffff
                AbstractGui.fill(matrixStack,
                        0,
                        0,
                        width,
                        height,
                        color
                )
            }
        }
    }

    @JvmStatic
    @SubscribeEvent
    fun renderBlockMarkings(event: RenderWorldLastEvent) {
        val mc = Minecraft.getInstance()
        val world = mc.world ?: return

        val renderPos = mc.gameRenderer.activeRenderInfo.projectedView

        WorldRendererAccess.getRenderInfos(event.context).forEach { renderChunk ->
            val chunk = world.getChunk(renderChunk.position)
            check(chunk is Chunk) { "chunk=$chunk should be a normal chunk" }

            val cap = chunk[BlockMarkingChunkCap.cap]
            check(cap != null) { "chunk=$chunk should have a capability" }
            event.matrixStack.push()
            event.matrixStack.translate(-renderPos.x, -renderPos.y, -renderPos.z)

            (cap.chunkBlockMarkings.values).forEach { marking ->
                renderBlockMarking(event.matrixStack, marking, event.partialTicks)
            }
            event.matrixStack.pop()
        }
    }

    private fun renderBlockMarking(matrixStack: MatrixStack, marking: BlockMarking, partialTicks: Float) {
        var rX = 0f
        var rY = 0f
        val rZ = 0f
        var pos = marking.pos + marking.associatedBlock.toVector()

        when (marking.side) {
            Direction.DOWN -> {
                rX = -90f
                pos += Vector3d(0.0, -0.001, 0.0)
            }
            Direction.UP -> {
                rX = 90f
                pos += Vector3d(0.0, 0.001, 0.0)
            }
            Direction.NORTH -> {
                pos += Vector3d(0.0, 0.0, -0.001)
            }
            Direction.SOUTH -> {
                rY = 180f
                pos += Vector3d(0.0, 0.0, 0.001)
            }
            Direction.WEST -> {
                rY = 90f
                pos += Vector3d(-0.01, 0.0, 0.0)
            }
            Direction.EAST -> {
                rY = -90f
                pos += Vector3d(0.01, 0.0, 0.0)
            }
        }

        RenderUtil.renderSquareInWorld(
                matrixStack,
                pos.x,
                pos.y,
                pos.z,
                -1,
                1,
                1,
                -1,
                rX,
                rY,
                rZ,
                partialTicks,
                marking.type.color)
    }

    @JvmStatic
    @SubscribeEvent
    fun handlePlayerFire(event: TickEvent.RenderTickEvent) {
        if (event.phase == TickEvent.Phase.END) {
            val mc = Minecraft.getInstance()
            val player = mc.player ?: return
            val world = mc.world ?: return
            val mainHandItem = player.getHeldItem(Hand.MAIN_HAND)

            if (!world.hasGunProperties) return

            if (mainHandItem.item is GunItem) {
                val gunCap = GunItemCap.forGunItem(mainHandItem)

                if (gunCap.isFiring) {
                    val gunProperties = world.getGunProperties(mainHandItem.item)
                    tryToFire(world, player, gunCap, gunProperties, event.renderTickTime)
                }
            }
        }
    }

    private fun tryToFire(world: ClientWorld, player: PlayerEntity, gunCap: GunItemCap, gunProperties: GunProperties, partialTicks: Float) {
        if (!gunCap.isShotAvailable(world, partialTicks)) return
        val lookVec = getLookVec(player.rotationYaw, player.rotationPitch)
        val startPos = player.getEyePosition(partialTicks)

        val shotResult =
                rayTraceWorldAndEntities(
                        world,
                        player,
                        startPos,
                        lookVec,
                        gunProperties.maxRange.toDouble(),
                        partialTicks)

        val message = ShotFiredMessage(
                partialTicks,
                player.rotationPitch,
                player.rotationYaw,
                world.gameTime,
                shotResult)

        CounterGuns.networkChannel.sendToServer(message)
    }
}