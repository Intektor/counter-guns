package de.intektor.counter_guns.client

import com.mojang.blaze3d.matrix.MatrixStack
import com.mojang.blaze3d.platform.GlStateManager
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.AbstractGui
import net.minecraft.client.renderer.Tessellator
import net.minecraft.client.renderer.vertex.DefaultVertexFormats
import net.minecraft.util.math.vector.Quaternion
import org.lwjgl.opengl.GL11
import java.awt.Color

object RenderUtil {

    fun renderSquareInWorld(
            matrixStack: MatrixStack,
            x: Double,
            y: Double,
            z: Double,
            left: Int,
            top: Int,
            right: Int,
            bottom: Int,
            rotationX: Float,
            rotationY: Float,
            rotationZ: Float,
            floatingTicks: Float,
            color: Int) {

        val mc = Minecraft.getInstance()
        val entity = mc.renderViewEntity ?: return

        matrixStack.push()

        matrixStack.translate(x, y, z)
        matrixStack.scale(0.05f, 0.05f, 0.05f)

        matrixStack.push()

        matrixStack.rotate(Quaternion(rotationX, 0f, 0f, true))
        matrixStack.rotate(Quaternion(0f, rotationY, 0f, true))
        matrixStack.rotate(Quaternion(0f, 0f, rotationZ, true))

        AbstractGui.fill(matrixStack, left, top, right, bottom, color)

        matrixStack.pop()

        matrixStack.pop()
    }
}