package de.intektor.counter_guns.config

import net.minecraftforge.common.ForgeConfigSpec

class CGConfig(private val heGrenadeWorldDamageSpec: ForgeConfigSpec.BooleanValue) {

    val dealHeWorldDamage: Boolean get() = heGrenadeWorldDamageSpec.get()
}