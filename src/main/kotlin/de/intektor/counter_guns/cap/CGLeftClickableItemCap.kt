package de.intektor.counter_guns.cap

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.item.LeftClickableItem
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.nbt.INBT
import net.minecraft.util.Direction
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityInject
import net.minecraftforge.common.capabilities.ICapabilityProvider
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.event.AttachCapabilitiesEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
class CGLeftClickableItemCap(private val stack: ItemStack) : ICapabilityProvider {

    companion object {
        @JvmStatic
        @CapabilityInject(CGLeftClickableItemCap::class)
        lateinit var cap: Capability<CGLeftClickableItemCap>

        val id = ResourceLocation(CounterGuns.ID, "cg_lci_cap")

        @JvmStatic
        @SubscribeEvent
        fun attackItemStackCap(evt: AttachCapabilitiesEvent<ItemStack>) {
            val stack = evt.`object`
            if (stack is ItemStack && stack.item is LeftClickableItem) {
                evt.addCapability(id, CGLeftClickableItemCap(stack))
            }
        }
    }

    private val lazyOptional = LazyOptional.of { this }

    /**
     * If this stack is currently being left clicked. Requires this stack to be in the main hand of the player
     */
    var isBeingLeftClicked = false

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        return if (cap === Companion.cap) lazyOptional.cast() else LazyOptional.empty()
    }

    class StorageProvider : Capability.IStorage<CGLeftClickableItemCap> {
        override fun writeNBT(capability: Capability<CGLeftClickableItemCap>?, instance: CGLeftClickableItemCap?, side: Direction?): INBT? {
            throw UnsupportedOperationException("Do not use this capability")
        }

        override fun readNBT(capability: Capability<CGLeftClickableItemCap>?, instance: CGLeftClickableItemCap?, side: Direction?, nbt: INBT?) {
            throw UnsupportedOperationException("Do not use this capability")
        }
    }
}