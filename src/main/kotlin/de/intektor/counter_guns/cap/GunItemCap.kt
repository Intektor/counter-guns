package de.intektor.counter_guns.cap

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.data.GunPropertyManager.Companion.getGunProperties
import de.intektor.counter_guns.item.GunItem
import net.minecraft.item.ItemStack
import net.minecraft.nbt.INBT
import net.minecraft.util.Direction
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityInject
import net.minecraftforge.common.capabilities.ICapabilityProvider
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.event.AttachCapabilitiesEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import kotlin.math.floor

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
class GunItemCap(private val stack: ItemStack) : ICapabilityProvider {

    companion object {
        @JvmStatic
        @CapabilityInject(GunItemCap::class)
        lateinit var cap: Capability<GunItemCap>

        val id = ResourceLocation(CounterGuns.ID, "cg_gun_item_cap")

        @JvmStatic
        @SubscribeEvent
        fun attackItemStackCap(evt: AttachCapabilitiesEvent<ItemStack>) {
            val stack = evt.`object`
            if (stack is ItemStack && stack.item is GunItem) {
                evt.addCapability(id, GunItemCap(stack))
            }
        }

        fun forGunItem(stack: ItemStack): GunItemCap {
            val cap = stack[cap]
            check(cap != null) { "stack=$stack has no gun capability attached" }
            return cap
        }
    }

    private val lazyOptional = LazyOptional.of { this }

    /**
     * If this gun is in active fire mode.
     * Accessed by [de.intektor.counter_guns.client.ClientEventHandler.handlePlayerFire]
     * to fire the shots every frame
     */
    var isFiring: Boolean = false

    /**
     * Every render
     */
    private var availableShots: Float = 0f
    private var lastPartialTicks: Float = 0f

    /**
     * Checks every render tick if a shot is available.
     * Every render tick the partial amount of the tick is added to [availableShots]
     * and if available shots is greater than 1, true is returned and 1 is subtracted from [availableShots]
     * and thus it is expected for a shot to be calculated and fired
     */
    fun isShotAvailable(world: World, currentPartialTicks: Float): Boolean {
        val properties = world.getGunProperties(stack.item)
        val partialTickDiff = currentPartialTicks - lastPartialTicks

        lastPartialTicks = currentPartialTicks

        availableShots += properties.shotsPerTick * partialTickDiff
        if (availableShots >= 1f) {
            availableShots--
            return true
        }
        return false
    }

    /**
     * Called every tick while in inventory.
     * Adds the remaining amount of [de.intektor.counter_guns.data.GunProperties.shotsPerTick]
     * to [availableShots] if is firing
     */
    fun inventoryTick(world: World) {
        val gunItem = stack.item

        val properties = world.getGunProperties(gunItem)

        if (isFiring) {
            val remainingPartialTicks = 1f - lastPartialTicks
            availableShots += properties.shotsPerTick * remainingPartialTicks

            lastPartialTicks = 0f
        }
    }

    /**
     * Checks if this gun can be fired this tick.
     * If true, the amount of shots that can be fired are subtracted from [availableShots]
     * @return the amount of shots that should be fired this tick
     */
    fun tryToFire(): Int {
        val amountOfReadyShots = floor(availableShots)
        availableShots -= amountOfReadyShots
        return amountOfReadyShots.toInt()
    }

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        return if (cap === Companion.cap) lazyOptional.cast() else LazyOptional.empty()
    }

    class StorageProvider : Capability.IStorage<GunItemCap> {
        override fun writeNBT(capability: Capability<GunItemCap>?, instance: GunItemCap?, side: Direction?): INBT? {
            throw UnsupportedOperationException("Do not use this capability")
        }

        override fun readNBT(capability: Capability<GunItemCap>?, instance: GunItemCap?, side: Direction?, nbt: INBT?) {
            throw UnsupportedOperationException("Do not use this capability")
        }
    }
}