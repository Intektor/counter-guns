package de.intektor.counter_guns.cap

import de.intektor.counter_guns.CounterGuns
import net.minecraft.entity.Entity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.nbt.INBT
import net.minecraft.util.Direction
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityInject
import net.minecraftforge.common.capabilities.ICapabilityProvider
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.event.AttachCapabilitiesEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import kotlin.math.*

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
internal class CounterGunsPlayerCap(private val player: PlayerEntity) : ICapabilityProvider {

    companion object {
        @JvmStatic
        @CapabilityInject(CounterGunsPlayerCap::class)
        lateinit var cap: Capability<CounterGunsPlayerCap>

        val id = ResourceLocation(CounterGuns.ID, "counter_guns_cap")

        @JvmStatic
        @SubscribeEvent
        fun attachEntityCaps(evt: AttachCapabilitiesEvent<Entity>) {
            val entity = evt.`object`
            if (entity is PlayerEntity) {
                evt.addCapability(id, CounterGunsPlayerCap(entity))
            }
        }
    }

    private val lazyOptional = LazyOptional.of { this }

    private var remainingFlashTicks = 0

    var isLeftClicking = false

    val currentFlashAmount: Float
        get() {
            //	1.88 full blindness
            if (remainingFlashTicks > 63) return 1.0f
            return (1 - cos(PI / 2 * (max(remainingFlashTicks, 0) / 63f))).pow(0.5).toFloat()
        }

    /**
     * Flash the player
     */
    fun flashPlayer(strength: Float) {
        //Only apply the new flash amount when the old one is smaller
        if (currentFlashAmount > strength) return

        remainingFlashTicks = (100 * strength).toInt()
    }

    fun onPlayerTick() {
        if (remainingFlashTicks > 0) {
            remainingFlashTicks--
        }
    }

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        return if (cap === Companion.cap) lazyOptional.cast() else LazyOptional.empty()
    }

    class StorageProvider : Capability.IStorage<CounterGunsPlayerCap> {
        override fun writeNBT(capability: Capability<CounterGunsPlayerCap>?, instance: CounterGunsPlayerCap?, side: Direction?): INBT? {
            throw UnsupportedOperationException("Do not use this capability")
        }

        override fun readNBT(capability: Capability<CounterGunsPlayerCap>?, instance: CounterGunsPlayerCap?, side: Direction?, nbt: INBT?) {
            throw UnsupportedOperationException("Do not use this capability")
        }
    }
}