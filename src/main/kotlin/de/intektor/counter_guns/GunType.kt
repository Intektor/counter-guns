package de.intektor.counter_guns

import de.intektor.counter_guns.item.CGItems
import de.intektor.counter_guns.item.GrenadeItem
import de.intektor.counter_guns.item.GunItem
import net.minecraftforge.fml.RegistryObject

internal enum class GunType(val id: String) {
    AK_47("ak_47"),
    AUG("aug"),
    AWP("awp"),
    DESERT_EAGLE("desert_eagle"),
    NOVA("nova"),
    P90("p90"),
    P250("p250"),
    SCAR_20("scar_20");

    fun item(): RegistryObject<GunItem> {
        return checkNotNull(CGItems.guns[this])
    }
}