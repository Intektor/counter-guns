package de.intektor.counter_guns.util

import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.vector.Vector3d

operator fun Vector3d.plus(other: Vector3d): Vector3d {
    return this.add(other)
}

operator fun Vector3d.minus(other: Vector3d): Vector3d {
    return this.subtract(other)
}

operator fun Vector3d.times(scalar: Double): Vector3d = this.scale(scalar)

operator fun Vector3d.times(factor: Vector3d): Vector3d = this.mul(factor)

fun BlockPos.toVector(): Vector3d = Vector3d(x.toDouble(), y.toDouble(), z.toDouble())