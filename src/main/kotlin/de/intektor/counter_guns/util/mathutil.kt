package de.intektor.counter_guns.util

fun lerp(current: Float, prev: Float, lerpFactor: Float): Float {
    return current + (current - prev) * lerpFactor
}