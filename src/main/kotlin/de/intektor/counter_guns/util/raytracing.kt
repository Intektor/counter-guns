package de.intektor.counter_guns.util

import de.intektor.counter_guns.net.to_server.ShotFiredMessage
import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.projectile.ProjectileHelper
import net.minecraft.util.math.RayTraceContext
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.World
import kotlin.math.cos
import kotlin.math.sin

/**
 * @see [net.minecraft.entity.Entity.pick]
 */
internal fun rayTraceWorldAndEntities(world: World,
                                      source: Entity,
                                      positionVec: Vector3d,
                                      lookVec: Vector3d,
                                      range: Double,
                                      partialTicks: Float): ShotFiredMessage.ShotData {
    val endVec = positionVec + lookVec.normalize() * range
    val rayTraceContext = RayTraceContext(
            positionVec,
            endVec,
            RayTraceContext.BlockMode.OUTLINE,
            RayTraceContext.FluidMode.NONE,
            null
    )
    val blockResult = world.rayTraceBlocks(rayTraceContext)

    val blockData = ShotFiredMessage.BlockShotData(blockResult.hitVec, blockResult.face, blockResult.pos)

    val entityResult = ProjectileHelper.rayTraceEntities(
            source,
            positionVec,
            endVec,
            cubeBoundingBox(positionVec, range),
            { entity -> entity is LivingEntity },
            range * range
    ) ?: return blockData

    val entityDistance = entityResult.hitVec.distanceTo(positionVec)
    val blockDistance = blockResult.hitVec.distanceTo(positionVec)

    return if (entityDistance < blockDistance) {
        ShotFiredMessage.EntityShotData(entityResult.hitVec, entityResult.entity.entityId, false)
    } else blockData
}

fun getLookVec(lookYaw: Float, lookPitch: Float): Vector3d {
    val pitchRadians = Math.toRadians(lookPitch.toDouble())
    val yawRadians = Math.toRadians(-lookYaw.toDouble())
    val f2 = cos(yawRadians)
    val f3 = sin(yawRadians)
    val f4 = cos(pitchRadians)
    val f5 = sin(pitchRadians)
    return Vector3d((f3 * f4), (-f5), (f2 * f4))
}