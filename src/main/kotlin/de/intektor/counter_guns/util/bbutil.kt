package de.intektor.counter_guns.util

import net.minecraft.util.math.AxisAlignedBB
import net.minecraft.util.math.vector.Vector3d

fun cubeBoundingBox(center: Vector3d, sideLength: Double): AxisAlignedBB {
    return AxisAlignedBB(
            center.x - sideLength / 2,
            center.y - sideLength / 2,
            center.z - sideLength / 2,
            center.x + sideLength / 2,
            center.y + sideLength / 2,
            center.z + sideLength / 2
    )
}