package de.intektor.counter_guns.block_markings

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.cap.get
import de.intektor.counter_guns.net.to_client.UpdateChunkBlockMarkingsMessage
import de.intektor.counter_guns.util.minus
import de.intektor.counter_guns.util.plus
import de.intektor.counter_guns.util.times
import de.intektor.counter_guns.util.toVector
import net.minecraft.util.Direction
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.BlockRayTraceResult
import net.minecraft.util.math.RayTraceContext
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.chunk.Chunk
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.fml.network.PacketDistributor
import java.util.*
import kotlin.math.cos
import kotlin.math.sin

/**
 * Util class for adding and removing block effects like blood or bullet holes
 */
internal object BlockMarkings {

    private val random = Random()

    fun addBlockMarkings(world: ServerWorld, markings: List<BlockMarking>) {
        markings
                .groupBy { world.getChunk(it.associatedBlock) }
                .forEach { (chunk, markings) ->
                    if (chunk !is Chunk) return

                    val cap = chunk[BlockMarkingChunkCap.cap]
                    check(cap != null) { "Chunk must not be null" }

                    val newMarkings = markings.map { Pair(it.uuid, it) }
                    cap.addAllMarkings(newMarkings)

                    val updateMessage = UpdateChunkBlockMarkingsMessage(chunk.pos.x, chunk.pos.z, emptyList(), newMarkings)
                    CounterGuns.networkChannel.send(PacketDistributor.TRACKING_CHUNK.with { chunk }, updateMessage)
                }
    }

    fun removeBlockMarkingsFromBlock(world: ServerWorld, blockPos: BlockPos) {
        val cap = BlockMarkingChunkCap.forBlock(world, blockPos)
        val idList = cap.removeMarkingsFromBlock(blockPos)

        val chunk = world.getChunkAt(blockPos)

        val msg = UpdateChunkBlockMarkingsMessage(chunk.pos.x, chunk.pos.z, idList, emptyList())
        CounterGuns.networkChannel.send(PacketDistributor.TRACKING_CHUNK.with { chunk }, msg)
    }

    /**
     * Gets called every tick this chunk is loaded on the server
     */
    fun tickChunkWithMarkings(chunk: Chunk, chunkCap: BlockMarkingChunkCap) {
        //only 1% of chunks are looked at every tick
        if (random.nextFloat() < 0.01) {
            //10% of all markings are removed
            val markingsToRemove = chunkCap.chunkBlockMarkings.values.filter { _ ->
                random.nextFloat() < 0.1f
            }.map { it.uuid }

            chunkCap.removeMarkingsWithId(markingsToRemove)

            val msg = UpdateChunkBlockMarkingsMessage(chunk.pos.x, chunk.pos.z, markingsToRemove, emptyList())
            CounterGuns.networkChannel.send(PacketDistributor.TRACKING_CHUNK.with { chunk }, msg)
        }
    }

    /**
     * Takes the incoming vector, rotates it bloodAmount times and puts it on surrounding blocks
     * @param hitPosition the position where the entity was hit
     * @param hitVector the direction the shot came from
     */
    fun splatterBlood(world: ServerWorld, hitPosition: Vector3d, hitVector: Vector3d, bloodAmount: Int) {
        val bloodMarkings = (0 until bloodAmount).mapNotNull { _ ->
            //15 degrees random rotation to every side
            val verticalRotation = Math.toRadians((random.nextFloat() - 0.5f) * 30.0)
            val horizontalRotation = Math.toRadians((random.nextFloat() - 0.5f) * 30.0)
            val bloodVec = (hitVector.normalize() + Vector3d(cos(horizontalRotation), sin(verticalRotation), sin(horizontalRotation))) * 7.0

            val result = world.rayTraceBlocks(RayTraceContext(
                    hitPosition,
                    hitPosition + bloodVec,
                    RayTraceContext.BlockMode.VISUAL,
                    RayTraceContext.FluidMode.NONE,
                    null))

            if (world.getBlockState(result.pos).isSolid) {
                calcBlockMarkingFromHit(result, BlockMarkingType.BLOOD)
            } else null
        }

        addBlockMarkings(world, bloodMarkings)
    }

    private fun calcBlockMarkingFromHit(result: BlockRayTraceResult, type: BlockMarkingType): BlockMarking {
        return calcBlockMarkingFromHit(result.pos, result.hitVec, result.face, type)
    }

    fun calcBlockMarkingFromHit(pos: BlockPos, hitVec: Vector3d, face: Direction, type: BlockMarkingType): BlockMarking {
        val relativePos = hitVec - pos.toVector()

        return BlockMarking(relativePos, face, pos, type, UUID.randomUUID())
    }
}