package de.intektor.counter_guns.block_markings

import de.intektor.counter_guns.net.readUUID
import de.intektor.counter_guns.net.writeUUID
import net.minecraft.network.PacketBuffer
import net.minecraft.util.Direction
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.vector.Vector2f
import net.minecraft.util.math.vector.Vector3d
import java.util.*

/**
 * @property pos relative coordinates to the block origin
 * @property side the side of the block the marking is on
 * @property associatedBlock the block this marking is associated with
 */
internal data class BlockMarking(val pos: Vector3d, val side: Direction, val associatedBlock: BlockPos, val type: BlockMarkingType, val uuid: UUID) {

    companion object {
        fun read(buf: PacketBuffer): BlockMarking {
            val x = buf.readDouble()
            val y = buf.readDouble()
            val z = buf.readDouble()
            val side = Direction.values()[buf.readByte().toInt()]
            val blockPos = buf.readBlockPos()
            val type = BlockMarkingType.values()[buf.readByte().toInt()]
            val uuid = buf.readUUID()
            return BlockMarking(Vector3d(x, y, z), side, blockPos, type, uuid)
        }
    }

    fun write(buf: PacketBuffer) {
        buf.writeDouble(pos.x)
        buf.writeDouble(pos.y)
        buf.writeDouble(pos.z)
        buf.writeByte(side.ordinal)
        buf.writeBlockPos(associatedBlock)
        buf.writeByte(type.ordinal)
        buf.writeUUID(uuid)
    }
}