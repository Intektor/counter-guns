package de.intektor.counter_guns.block_markings

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.cap.get
import de.intektor.counter_guns.net.to_client.UpdateChunkBlockMarkingsMessage
import net.minecraft.nbt.*
import net.minecraft.util.Direction
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.IWorld
import net.minecraft.world.chunk.Chunk
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityInject
import net.minecraftforge.common.capabilities.ICapabilitySerializable
import net.minecraftforge.common.util.Constants
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.event.AttachCapabilitiesEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.network.PacketDistributor
import java.util.*

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
internal class BlockMarkingChunkCap(private val chunk: Chunk) : ICapabilitySerializable<CompoundNBT> {

    companion object {

        private const val TAG_BLOCK_MARKING_LIST = "block_marking_list"
        private const val TAG_BLOCK_MARKING_POS_X = "p_x"
        private const val TAG_BLOCK_MARKING_POS_Y = "p_y"
        private const val TAG_BLOCK_MARKING_POS_Z = "p_z"
        private const val TAG_BLOCK_MARKING_BLOCK_POS_X = "b_p_x"
        private const val TAG_BLOCK_MARKING_BLOCK_POS_Y = "b_p_y"
        private const val TAG_BLOCK_MARKING_BLOCK_POS_Z = "b_p_z"
        private const val TAG_BLOCK_SIDE = "b_s"
        private const val TAG_MARKING_TYPE = "m_t"

        @JvmStatic
        @CapabilityInject(BlockMarkingChunkCap::class)
        lateinit var cap: Capability<BlockMarkingChunkCap>

        val id = ResourceLocation(CounterGuns.ID, "cg_block_marking")

        @JvmStatic
        @SubscribeEvent
        fun attachChunkCap(evt: AttachCapabilitiesEvent<Chunk>) {
            val chunk = evt.`object`
            if (chunk is Chunk) {
                evt.addCapability(id, BlockMarkingChunkCap(chunk))
            }
        }

        fun forBlock(world: IWorld, blockPos: BlockPos): BlockMarkingChunkCap {
            val chunk = world.getChunk(blockPos)
            check(chunk is Chunk) { "chunk=$chunk at pos=$blockPos should be a chunk" }

            val capability = chunk[cap]
            check(capability != null) { "chunk=$chunk at pos=$blockPos should have a capability" }
            return capability
        }
    }

    private val lazyOptional = LazyOptional.of { this }

    val chunkBlockMarkings = mutableMapOf<UUID, BlockMarking>()
    private val markingForBlockPos = mutableMapOf<BlockPos, MutableList<BlockMarking>>()

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        return if (cap === BlockMarkingChunkCap.cap) lazyOptional.cast() else LazyOptional.empty()
    }

    fun addAllMarkings(markings: Map<UUID, BlockMarking>) {
        addAllMarkings(markings.entries.map { Pair(it.key, it.value) })
    }

    fun addAllMarkings(markings: List<Pair<UUID, BlockMarking>>) {
        chunkBlockMarkings += markings
        markings.map { it.second }.groupBy { it.associatedBlock }.forEach { (pos, markings) ->
            if (pos !in markingForBlockPos) {
                markingForBlockPos[pos] = markings.toMutableList()
            } else {
                markingForBlockPos[pos]?.addAll(markings)
            }
        }
    }

    fun removeMarkingsWithId(markings: List<UUID>) {
        markings
                .mapNotNull { chunkBlockMarkings[it] }
                .map {
                    val blockMarkingList = markingForBlockPos[it.associatedBlock]
                    check(blockMarkingList != null) { "block pos=${it.associatedBlock} should have a list in the first place" }
                    blockMarkingList.remove(it)
                    it
                }
                .also {
                    chunkBlockMarkings.values.removeAll(it)
                }
    }

    /**
     * Removes all markings from the given block
     * @return the ids of the markings that have been removed
     */
    fun removeMarkingsFromBlock(blockPos: BlockPos): List<UUID> {
        val markingsOnBlock = markingForBlockPos[blockPos] ?: return emptyList()
        chunkBlockMarkings.values.removeAll(markingsOnBlock)

        val idList = markingsOnBlock.map { it.uuid }
        markingsOnBlock.clear()
        return idList
    }

    override fun serializeNBT(): CompoundNBT {
        val nbt = CompoundNBT()
        val listNbt = ListNBT()
        chunkBlockMarkings.values.forEach { blockMarking ->
            val markingNbt = CompoundNBT()
            markingNbt.putDouble(TAG_BLOCK_MARKING_POS_X, blockMarking.pos.x)
            markingNbt.putDouble(TAG_BLOCK_MARKING_POS_Y, blockMarking.pos.y)
            markingNbt.putDouble(TAG_BLOCK_MARKING_POS_Z, blockMarking.pos.z)
            markingNbt.putByte(TAG_BLOCK_MARKING_BLOCK_POS_X, (blockMarking.associatedBlock.x - chunk.pos.xStart).toByte())
            markingNbt.putByte(TAG_BLOCK_MARKING_BLOCK_POS_Z, (blockMarking.associatedBlock.z - chunk.pos.zStart).toByte())
            markingNbt.putByte(TAG_BLOCK_MARKING_BLOCK_POS_Y, blockMarking.associatedBlock.y.toByte())
            markingNbt.putByte(TAG_BLOCK_SIDE, blockMarking.side.ordinal.toByte())
            markingNbt.putByte(TAG_MARKING_TYPE, blockMarking.type.ordinal.toByte())
            listNbt.add(markingNbt)
        }

        nbt.put(TAG_BLOCK_MARKING_LIST, listNbt)
        return nbt
    }

    override fun deserializeNBT(nbt: CompoundNBT?) {
        if (nbt == null) return

        val markingsList = nbt.getList(TAG_BLOCK_MARKING_LIST, Constants.NBT.TAG_COMPOUND)
        val loadedMarkings = (0 until markingsList.size).map { i ->
            val markingNbt = markingsList.getCompound(i)
            val x = markingNbt.getDouble(TAG_BLOCK_MARKING_POS_X)
            val y = markingNbt.getDouble(TAG_BLOCK_MARKING_POS_Y)
            val z = markingNbt.getDouble(TAG_BLOCK_MARKING_POS_Z)
            val posX = chunk.pos.xStart + markingNbt.getByte(TAG_BLOCK_MARKING_BLOCK_POS_X)
            val poxY = markingNbt.getByte(TAG_BLOCK_MARKING_BLOCK_POS_Y).toInt()
            val posZ = chunk.pos.zStart + markingNbt.getByte(TAG_BLOCK_MARKING_BLOCK_POS_Z)
            val side = Direction.values()[markingNbt.getByte(TAG_BLOCK_SIDE).toInt()]
            val type = BlockMarkingType.values()[markingNbt.getByte(TAG_MARKING_TYPE).toInt()]

            val uuidForMarking = UUID.randomUUID()
            uuidForMarking to BlockMarking(Vector3d(x, y, z), side, BlockPos(posX, poxY, posZ), type, uuidForMarking)
        }

        addAllMarkings(loadedMarkings)
    }

    class StorageProvider : Capability.IStorage<BlockMarkingChunkCap> {
        override fun writeNBT(capability: Capability<BlockMarkingChunkCap>?, instance: BlockMarkingChunkCap?, side: Direction?): INBT? {
            throw UnsupportedOperationException("Do not use this capability")
        }

        override fun readNBT(capability: Capability<BlockMarkingChunkCap>?, instance: BlockMarkingChunkCap?, side: Direction?, nbt: INBT?) {
            throw UnsupportedOperationException("Do not use this capability")
        }
    }
}