package de.intektor.counter_guns.block_markings

import de.intektor.counter_guns.cap.get
import net.minecraft.client.Minecraft

internal object ClientMarkingUtil {
    fun getChunkCapability(chunkX: Int, chunkY: Int): BlockMarkingChunkCap {
        val mc = Minecraft.getInstance()
        val world = mc.world
        check(world != null) { "world should not be null here" }

        val chunk = world.getChunk(chunkX, chunkY)
        val blockMarkingCap = chunk[BlockMarkingChunkCap.cap]
        check(blockMarkingCap != null) { "this chunk=$chunk should have a capability" }

        return blockMarkingCap
    }
}