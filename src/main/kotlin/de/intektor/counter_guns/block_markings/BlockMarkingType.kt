package de.intektor.counter_guns.block_markings

import de.intektor.counter_guns.CounterGuns
import net.minecraft.nbt.CompoundNBT
import net.minecraftforge.common.capabilities.ICapabilitySerializable
import net.minecraftforge.fml.common.Mod

enum class BlockMarkingType(val color: Int) {
    BLOOD(0xffff0000.toInt()),
    BULLET_HOLE(0xff000000.toInt())
}