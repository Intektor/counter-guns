package de.intektor.counter_guns

import de.intektor.counter_guns.item.CGItems
import de.intektor.counter_guns.item.GrenadeItem
import net.minecraftforge.fml.RegistryObject

internal enum class GrenadeType(val id: String) {
    SMOKE("smoke_grenade"),
    INCENDIARY("incendiary_grenade"),
    HIGH_EXPLOSIVE("he_grenade"),
    FLASH("flash_grenade");

    fun item(): RegistryObject<GrenadeItem> {
        return checkNotNull(CGItems.grenades[this])
    }
}