package de.intektor.counter_guns

import de.intektor.counter_guns.block_markings.BlockMarkingType
import de.intektor.counter_guns.block_markings.BlockMarkings
import de.intektor.counter_guns.cap.GunItemCap
import de.intektor.counter_guns.data.GunPropertyManager.Companion.getGunProperties
import de.intektor.counter_guns.item.GunItem
import de.intektor.counter_guns.net.to_server.ShotFiredMessage
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.util.DamageSource
import net.minecraft.util.Hand
import net.minecraft.world.server.ServerWorld

internal object GunShots {

    fun processShotFromClient(player: ServerPlayerEntity, world: ServerWorld, message: ShotFiredMessage) {
        val shotData = message.shotData

        val itemMainHand = player.getHeldItem(Hand.MAIN_HAND)
        if (itemMainHand.item !is GunItem) return

        val gunProperties = player.world.getGunProperties(itemMainHand.item)

        if (shotData is ShotFiredMessage.BlockShotData) {
            val marking = BlockMarkings.calcBlockMarkingFromHit(
                    shotData.blockPos,
                    shotData.hitPos,
                    shotData.side,
                    BlockMarkingType.BULLET_HOLE)

            BlockMarkings.addBlockMarkings(world, listOf(marking))
        } else if (shotData is ShotFiredMessage.EntityShotData) {
            val hitEntity = world.getEntityByID(shotData.entityId) ?: return

            val distanceScale = player.getDistance(hitEntity) / gunProperties.maxRange

            val damage =
                    (if (shotData.headshot) gunProperties.headshotDamage else gunProperties.damage) * distanceScale

            if (hitEntity is LivingEntity) {
                hitEntity.attackEntityFrom(DamageSource.LAVA, damage)
            }
        }
    }
}