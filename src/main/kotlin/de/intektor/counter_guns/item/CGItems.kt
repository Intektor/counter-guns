package de.intektor.counter_guns.item

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.GrenadeType
import de.intektor.counter_guns.GunType
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.RegistryObject
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries

internal object CGItems {

    val items: DeferredRegister<Item> = DeferredRegister.create(ForgeRegistries.ITEMS, CounterGuns.ID)

    val group = object : ItemGroup(CounterGuns.ID) {
        override fun createIcon(): ItemStack = ItemStack(GrenadeType.HIGH_EXPLOSIVE.item().get())
    }

    val grenades: Map<GrenadeType, RegistryObject<GrenadeItem>> = GrenadeType.values().associate { grenadeType ->
        grenadeType to items.register(grenadeType.id) {
            GrenadeItem(Item.Properties().also {
                it.maxStackSize(8)
                it.group(group)
            }, grenadeType)
        }
    }

    val guns: Map<GunType, RegistryObject<GunItem>> = GunType.values().associate { gunType ->
        gunType to items.register(gunType.id) {
            GunItem(Item.Properties().also {
                it.maxStackSize(1)
                it.group(group)
            }, gunType)
        }
    }
}