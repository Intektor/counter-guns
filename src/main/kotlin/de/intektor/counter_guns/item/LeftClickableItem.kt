package de.intektor.counter_guns.item

import de.intektor.counter_guns.cap.CGLeftClickableItemCap
import de.intektor.counter_guns.cap.CounterGunsPlayerCap
import de.intektor.counter_guns.cap.get
import net.minecraft.entity.Entity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.Hand
import net.minecraft.world.World

/**
 * Item that can be left clicked synced by [de.intektor.counter_guns.cap.CounterGunsPlayerCap]
 * and [de.intektor.counter_guns.net.to_server.PlayerLeftClickMessage]
 * @author Intektor
 */
internal abstract class LeftClickableItem(properties: Properties) : Item(properties) {

    override fun inventoryTick(stack: ItemStack, worldIn: World, entityIn: Entity, itemSlot: Int, isSelected: Boolean) {
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected)

        val stackCap = stack[CGLeftClickableItemCap.cap]
        if (stackCap == null) {
            println("There should be a capability on this stack=$stack. This is an error")
            return
        }

        //We are only interested in players holding these items for this mod
        if (entityIn !is PlayerEntity) return
        val playerCap = entityIn[CounterGunsPlayerCap.cap] ?: return

        val isHeldInMainHand = entityIn.getHeldItem(Hand.MAIN_HAND) == stack
        if (isHeldInMainHand) {
            if (playerCap.isLeftClicking) {
                if (!stackCap.isBeingLeftClicked) {
                    //Is held in the main hand, the player is left clicking, but this stack is not being left clicked
                    //such this is the first tick it is being clicked
                    stackCap.isBeingLeftClicked = true
                    onLeftClickingStarted(worldIn, entityIn, stack)
                }
                onLeftClicking(worldIn, entityIn, stack)
            } else if (stackCap.isBeingLeftClicked) {
                //The stack is being left clicked, but the player isn't left clicking, such stop the clicking
                stackCap.isBeingLeftClicked = false
                onLeftClickingStopped(worldIn, entityIn, stack)
            }
        } else if (stackCap.isBeingLeftClicked) {
            //The stack was left clicked, but is no longer in the main hand
            //The player probably switched slots so we need to stop the left clicking
            stackCap.isBeingLeftClicked = false
            onLeftClickingStopped(worldIn, entityIn, stack)
        }
    }

    /**
     * Called the first tick when started clicking and a previous clicking was already stopped
     * on both server and client
     */
    open fun onLeftClickingStarted(world: World, player: PlayerEntity, stack: ItemStack) {

    }

    /**
     * Called when clicking, [onLeftClickingStarted] will have been called before
     * on both server and client
     */
    open fun onLeftClicking(world: World, player: PlayerEntity, stack: ItemStack) {

    }

    /**
     * Called the last tick when started clicking and a previous clicking was already stopped
     * on both server and client. Called after [onLeftClicking]
     */
    open fun onLeftClickingStopped(world: World, player: PlayerEntity, stack: ItemStack) {

    }
}