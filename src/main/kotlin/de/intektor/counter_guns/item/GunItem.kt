package de.intektor.counter_guns.item

import de.intektor.counter_guns.GunType
import de.intektor.counter_guns.cap.GunItemCap
import de.intektor.counter_guns.data.GunPropertyManager.Companion.gunPropertyManager
import de.intektor.counter_guns.data.GunPropertyManager.Companion.hasGunProperties
import net.minecraft.block.BlockState
import net.minecraft.entity.Entity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.UseAction
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.Util
import net.minecraft.util.math.BlockPos
import net.minecraft.util.text.StringTextComponent
import net.minecraft.world.World
import net.minecraft.world.server.ServerWorld

internal class GunItem(properties: Properties, private val gunType: GunType) : LeftClickableItem(properties) {

    override fun inventoryTick(stack: ItemStack, worldIn: World, entityIn: Entity, itemSlot: Int, isSelected: Boolean) {
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected)

        if (!worldIn.hasGunProperties) return

        val cap = GunItemCap.forGunItem(stack)
        cap.inventoryTick(worldIn)
    }

    override fun onLeftClickingStarted(world: World, player: PlayerEntity, stack: ItemStack) {
        super.onLeftClickingStarted(world, player, stack)
        val cap = GunItemCap.forGunItem(stack)
        cap.isFiring = true
    }

    override fun onLeftClickingStopped(world: World, player: PlayerEntity, stack: ItemStack) {
        super.onLeftClickingStopped(world, player, stack)
        val cap = GunItemCap.forGunItem(stack)
        cap.isFiring = false
    }

    override fun canPlayerBreakBlockWhileHolding(state: BlockState, worldIn: World, pos: BlockPos, player: PlayerEntity): Boolean {
        return false
    }

    override fun getUseAction(stack: ItemStack): UseAction {
        return UseAction.NONE
    }

    override fun getUseDuration(stack: ItemStack): Int {
        return Integer.MAX_VALUE
    }

    override fun onItemRightClick(world: World, player: PlayerEntity, handIn: Hand): ActionResult<ItemStack> {
        player.activeHand = handIn
        return ActionResult.resultConsume(player.getHeldItem(handIn))
    }
}