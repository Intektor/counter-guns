package de.intektor.counter_guns.item

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.GrenadeType
import de.intektor.counter_guns.entity.GrenadeEntity
import net.minecraft.block.BlockState
import net.minecraft.entity.Entity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.projectile.ArrowEntity
import net.minecraft.item.ItemStack
import net.minecraft.item.UseAction
import net.minecraft.util.ActionResult
import net.minecraft.util.ActionResultType
import net.minecraft.util.Hand
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

internal class GrenadeItem(properties: Properties, val grenadeType: GrenadeType) : LeftClickableItem(properties) {

    override fun onLeftClickingStarted(world: World, player: PlayerEntity, stack: ItemStack) {
    }

    override fun onLeftClicking(world: World, player: PlayerEntity, stack: ItemStack) {
    }

    override fun onLeftClickingStopped(world: World, player: PlayerEntity, stack: ItemStack) {
        spawnGrenade(world, player, stack, 1.5f)
    }

    override fun onItemRightClick(world: World, player: PlayerEntity, handIn: Hand): ActionResult<ItemStack> {
        player.activeHand = handIn
        return ActionResult.resultConsume(player.getHeldItem(handIn))
    }

    override fun canPlayerBreakBlockWhileHolding(state: BlockState, worldIn: World, pos: BlockPos, player: PlayerEntity): Boolean {
        return false
    }

    override fun itemInteractionForEntity(stack: ItemStack, playerIn: PlayerEntity, target: LivingEntity, hand: Hand): ActionResultType {
        return ActionResultType.FAIL
    }

    override fun getUseAction(stack: ItemStack): UseAction {
        return UseAction.NONE
    }

    override fun getUseDuration(stack: ItemStack): Int {
        return Integer.MAX_VALUE
    }

    override fun onPlayerStoppedUsing(stack: ItemStack, worldIn: World, entityLiving: LivingEntity, timeLeft: Int) {
        super.onPlayerStoppedUsing(stack, worldIn, entityLiving, timeLeft)

        if (entityLiving is PlayerEntity) {
            spawnGrenade(worldIn, entityLiving, stack, 0.0f)
        }
    }

    private fun spawnGrenade(world: World, player: PlayerEntity, stack: ItemStack, velocity: Float) {
        if (!world.isRemote) {
            val grenade = GrenadeEntity(CounterGuns.entityGrenade.get(), world, grenadeType)
            grenade.setPosition(player.posX, player.posY + player.eyeHeight, player.posZ)
            grenade.func_234612_a_(player, player.rotationPitch, player.rotationYaw, 0.0f, velocity, 1.0f)
            grenade.setShooter(player)

            if (world.addEntity(grenade)) {
                if (!player.isCreative) stack.shrink(1)
            }
        }
    }
}