package de.intektor.counter_guns.data.gen

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.GrenadeType
import de.intektor.counter_guns.GunType
import de.intektor.counter_guns.data.GunProperties
import de.intektor.counter_guns.data.GunScope
import de.intektor.counter_guns.item.CGItems
import de.intektor.counter_guns.item.GunItem
import net.minecraft.data.DataGenerator
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.generators.ExistingFileHelper
import net.minecraftforge.client.model.generators.ItemModelProvider
import net.minecraftforge.client.model.generators.ModelBuilder
import net.minecraftforge.common.data.LanguageProvider
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.MOD)
internal object DataGens {

    @JvmStatic
    @SubscribeEvent
    fun gather(evt: GatherDataEvent) {
        arrayOf(
                ItemModels(evt.generator, evt.existingFileHelper),
                Lang(evt.generator, "en_us"),
                GunProps(evt.generator)
        ).forEach { evt.generator.addProvider(it) }
    }

    private class Lang(gen: DataGenerator, locale: String) : LanguageProvider(gen, CounterGuns.ID, locale) {
        override fun addTranslations() {
            add("itemGroup.${CGItems.group.path}", "Counter Guns")
            addItem(GrenadeType.FLASH.item(), "Flashbang")
            addItem(GrenadeType.HIGH_EXPLOSIVE.item(), "HE Grenade")
            addItem(GrenadeType.INCENDIARY.item(), "Incendiary Grenade")
            addItem(GrenadeType.SMOKE.item(), "Smoke Grenade")
        }
    }

    private class ItemModels(
            generator: DataGenerator,
            existingFileHelper: ExistingFileHelper
    ) : ItemModelProvider(generator, CounterGuns.ID, existingFileHelper) {

        override fun registerModels() {
            GrenadeType.values().forEach { grenadeType ->
                withExistingParent(grenadeType.item().id.path, ResourceLocation("minecraft", "item/generated"))
                        .texture("layer0", ResourceLocation(CounterGuns.ID, "items/${grenadeType.id}"))
            }

            gunModelBase(GunType.AK_47)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, 0f, 0f)
                            .scale(1.6f, 1.6f, 1.6f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(1f, 1f, 1f)
                        .end()
                    .end()
                    // @formatter:on

            gunModelBase(GunType.AUG)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, -1f, 2.4f)
                            .scale(1.1f, 1.1f, 1.1f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.88f, 0.88f, 0.88f)
                        .end()
                    .end()
            // @formatter:on

            gunModelBase(GunType.AWP)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, 0f, -1f)
                            .scale(1.9f, 1.9f, 1.9f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.88f, 0.88f, 0.88f)
                        .end()
                    .end()
            // @formatter:on

            gunModelBase(GunType.DESERT_EAGLE)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, -0.4f, -1f)
                            .scale(0.7f, 0.7f, 0.7f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.88f, 0.88f, 0.88f)
                        .end()
                    .end()
            // @formatter:on

            gunModelBase(GunType.NOVA)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, 0f, 0f)
                            .scale(1.6f, 1.6f, 1.6f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(1f, 1f, 1f)
                        .end()
                    .end()
            // @formatter:on

             gunModelBase(GunType.NOVA)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, 0f, 0f)
                            .scale(1.6f, 1.6f, 1.6f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(1f, 1f, 1f)
                        .end()
                    .end()
            // @formatter:on

             gunModelBase(GunType.P90)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, -1f, 2.4f)
                            .scale(1.1f, 1.1f, 1.1f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.88f, 0.88f, 0.88f)
                        .end()
                    .end()
            // @formatter:on

             gunModelBase(GunType.P250)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, -0.5f, -1f)
                            .scale(0.7f, 0.7f, 0.7f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.88f, 0.88f, 0.88f)
                        .end()
                    .end()
            // @formatter:on

             gunModelBase(GunType.SCAR_20)
                    // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.THIRDPERSON_RIGHT)
                            .rotation(-90f, 260f, -90f)
                            .translation(0f, -1f, 1.2f)
                            .scale(1.7f, 1.7f, 1.7f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, -100f, 0f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(1.2f, 1.2f, 1.2f)
                        .end()
                    .end()
            // @formatter:on
        }

        private fun gunModelBase(gunType: GunType) =
                withExistingParent(gunType.item().id.path, ResourceLocation("minecraft", "item/generated"))
                        .texture("layer0", ResourceLocation(CounterGuns.ID, "items/${gunType.id}"))
    }

    private class GunProps(gen: DataGenerator) : GunPropertiesProvider(gen, CounterGuns.ID) {
        override fun addProperties() {
            add(GunType.AK_47.item(), GunProperties(
                    fireRate = 10f,
                    fullAuto = true,
                    damage = 3.6f,
                    headshotDamage = 6.0f,
                    scopes = emptyList(),
                    bulletsPerShot = 1,
                    maxRange = 164f
            ))
            add(GunType.AUG.item(), GunProperties(
                    fireRate = 10f,
                    fullAuto = true,
                    damage = 2.8f,
                    headshotDamage = 6.0f,
                    scopes = listOf(GunScope(45)),
                    bulletsPerShot = 1,
                    maxRange = 164f
            ))
            add(GunType.AWP.item(), GunProperties(
                    fireRate = 0.66f,
                    fullAuto = false,
                    damage = 11.5f,
                    headshotDamage = 6.0f,
                    scopes = listOf(GunScope(40), GunScope(10)),
                    bulletsPerShot = 1,
                    maxRange = 164f
            ))
            add(GunType.DESERT_EAGLE.item(), GunProperties(
                    fireRate = 5f,
                    fullAuto = false,
                    damage = 6.3f,
                    headshotDamage = 6.0f,
                    scopes = emptyList(),
                    bulletsPerShot = 1,
                    maxRange = 82f
            ))
            add(GunType.NOVA.item(), GunProperties(
                    fireRate = 1.25f,
                    fullAuto = false,
                    damage = 2.6f,
                    headshotDamage = 6.0f,
                    scopes = emptyList(),
                    bulletsPerShot = 9,
                    maxRange = 60f
            ))
            add(GunType.P250.item(), GunProperties(
                    fireRate = 6.67f,
                    fullAuto = false,
                    damage = 3.8f,
                    headshotDamage = 6.0f,
                    scopes = emptyList(),
                    bulletsPerShot = 1,
                    maxRange = 82f
            ))
            add(GunType.P90.item(), GunProperties(
                    fireRate = 14.29f,
                    fullAuto = true,
                    damage = 2.6f,
                    headshotDamage = 6.0f,
                    scopes = emptyList(),
                    bulletsPerShot = 1,
                    maxRange = 74f
            ))
            add(GunType.SCAR_20.item(), GunProperties(
                    fireRate = 4f,
                    fullAuto = true,
                    damage = 8.0f,
                    headshotDamage = 6.0f,
                    scopes = listOf(GunScope(40), GunScope(15)),
                    bulletsPerShot = 1,
                    maxRange = 164f
            ))
        }
    }

}