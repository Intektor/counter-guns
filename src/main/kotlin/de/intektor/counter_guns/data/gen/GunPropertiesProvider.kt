package de.intektor.counter_guns.data.gen

import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.data.GunProperties
import de.intektor.counter_guns.data.GunPropertyManager
import net.minecraft.data.DataGenerator
import net.minecraft.data.DirectoryCache
import net.minecraft.data.IDataProvider
import net.minecraft.item.Item
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.RegistryObject
import java.nio.file.Files
import java.nio.file.Path

abstract class GunPropertiesProvider(
    private val gen: DataGenerator,
    private val modId: String
) : IDataProvider {

    private val data = HashMap<ResourceLocation, GunProperties>()

    protected fun add(rl: ResourceLocation, properties: GunProperties) {
        require(data.putIfAbsent(rl, properties) == null) {
            "Duplicate properties for gun: $rl"
        }
    }

    protected fun add(item: RegistryObject<out Item>, properties: GunProperties) {
        add(item.id, properties)
    }

    protected abstract fun addProperties()

    final override fun act(cache: DirectoryCache) {
        addProperties()
        if (data.isNotEmpty()) {
            for ((gunId, properties) in data) {
                val path = gen.outputFolder.resolve(GunPropertyManager.pathForProperties(gunId))
                save(cache, properties, path)
            }
        }
    }

    override fun getName(): String = "CounterGuns gun properties: $modId"

    private fun save(cache: DirectoryCache, properties: GunProperties, path: Path) {
        val data = GunPropertyManager.toJson(properties)
        val hash = IDataProvider.HASH_FUNCTION.hashUnencodedChars(data).toString()
        if (hash != cache.getPreviousHash(path) || !Files.exists(path)) {
            Files.createDirectories(path.parent)
            Files.newBufferedWriter(path).use { writer ->
                writer.write(data)
            }
        }

        cache.recordHash(path, hash)
    }

}