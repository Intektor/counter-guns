package de.intektor.counter_guns.data

import de.intektor.counter_guns.data.GunScope.Companion.readGunScope
import de.intektor.counter_guns.data.GunScope.Companion.writeGunScope
import de.intektor.counter_guns.net.readList
import de.intektor.counter_guns.net.writeCollection
import net.minecraft.network.PacketBuffer

/**
 * @property fireRate how often the gun fires every second
 * @property bulletsPerShot the amount of bullets shot every every shot (relevant for shotguns)
 * @property maxRange how far the bullet flies at max. Damage drop off is scaled linearly
 */
data class GunProperties(
        val fireRate: Float,
        val fullAuto: Boolean,
        val damage: Float,
        val headshotDamage: Float,
        val scopes: List<GunScope>,
        val bulletsPerShot: Int,
        val maxRange: Float
) {

    val shotsPerTick: Float
        get() = fireRate / 20f

    companion object {
        fun PacketBuffer.writeGunProperties(properties: GunProperties) {
            with(properties) {
                writeFloat(fireRate)
                writeBoolean(fullAuto)
                writeFloat(damage)
                writeFloat(headshotDamage)
                writeCollection(scopes) {
                    writeGunScope(it)
                }
                writeVarInt(bulletsPerShot)
                writeFloat(maxRange)
            }
        }

        fun PacketBuffer.readGunProperties(): GunProperties {
            return GunProperties(
                    fireRate = readFloat(),
                    fullAuto = readBoolean(),
                    damage = readFloat(),
                    headshotDamage = readFloat(),
                    scopes = readList {
                        readGunScope()
                    },
                    bulletsPerShot = readVarInt(),
                    maxRange = readFloat()
            )
        }
    }
}

data class GunScope(val zoom: Int) {

    companion object {
        fun PacketBuffer.writeGunScope(scope: GunScope) {
            with(scope) {
                writeVarInt(zoom)
            }
        }

        fun PacketBuffer.readGunScope(): GunScope {
            return GunScope(
                    zoom = readVarInt()
            )
        }
    }

}
