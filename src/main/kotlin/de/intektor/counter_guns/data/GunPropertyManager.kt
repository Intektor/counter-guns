package de.intektor.counter_guns.data

import com.google.common.collect.MapMaker
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import de.intektor.counter_guns.CounterGuns
import de.intektor.counter_guns.net.to_client.GunPropertiesMessage
import net.minecraft.client.resources.JsonReloadListener
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.item.Item
import net.minecraft.profiler.IProfiler
import net.minecraft.resources.DataPackRegistries
import net.minecraft.resources.IResourceManager
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.client.event.ClientPlayerNetworkEvent
import net.minecraftforge.event.AddReloadListenerEvent
import net.minecraftforge.event.entity.player.PlayerEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.thread.EffectiveSide
import net.minecraftforge.fml.network.NetworkDirection
import net.minecraftforge.fml.network.PacketDistributor
import net.minecraftforge.fml.server.ServerLifecycleHooks
import java.util.concurrent.ConcurrentMap

@Mod.EventBusSubscriber(modid = CounterGuns.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
class GunPropertyManager private constructor() : JsonReloadListener(gson, "${CounterGuns.ID}/gun_properties") {

    private var properties: Map<ResourceLocation, GunProperties>? = null

    private fun requireInitialized(): Map<ResourceLocation, GunProperties> {
        return checkNotNull(properties) {
            "GunPropertyManager not initialized"
        }
    }

    override fun apply(objectIn: Map<ResourceLocation, JsonElement>, resourceManagerIn: IResourceManager, profilerIn: IProfiler) {
        properties = objectIn.mapValues {
            gson.fromJson(it.value, GunProperties::class.java)
        }
        if (EffectiveSide.get().isServer && ServerLifecycleHooks.getCurrentServer() != null) {
            CounterGuns.networkChannel.send(PacketDistributor.ALL.noArg(), createSyncMessage())
        }
    }

    fun injectServerValues(map: Map<ResourceLocation, GunProperties>) {
        this.properties = map
    }

    fun clear() {
        this.properties = emptyMap()
    }

    private fun createSyncMessage(): GunPropertiesMessage {
        return GunPropertiesMessage(requireInitialized().toMap())
    }

    operator fun get(id: ResourceLocation): GunProperties? = requireInitialized()[id]

    companion object {

        val clientSideInstance = GunPropertyManager()

        private val gson = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()

        private val map: ConcurrentMap<DataPackRegistries, GunPropertyManager> = MapMaker().weakKeys().makeMap()

        fun toJson(properties: GunProperties): String {
            return gson.toJson(properties)
        }

        fun pathForProperties(itemId: ResourceLocation): String {
            return "data/${itemId.namespace}/${CounterGuns.ID}/gun_properties/${itemId.path}.json"
        }

        @SubscribeEvent
        @JvmStatic
        fun playerLogin(evt: PlayerEvent.PlayerLoggedInEvent) {
            val player = evt.player as? ServerPlayerEntity ?: return
            val message = evt.player.server?.dataPackRegistries?.gunPropertyManager?.createSyncMessage() ?: return
            CounterGuns.networkChannel.sendTo(message, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT)
        }

        @JvmStatic
        @SubscribeEvent
        fun datapackRegistry(event: AddReloadListenerEvent) {
            val manager = GunPropertyManager()
            check(map.putIfAbsent(event.dataPackRegistries, manager) == null) {
                "Duplicate DataPackRegistries"
            }
            event.addListener(manager)
        }

        @JvmStatic
        @SubscribeEvent
        fun clientDisconnect(event: ClientPlayerNetworkEvent.LoggedOutEvent) {
            clientSideInstance.clear()
        }

        val World.gunPropertyManager: GunPropertyManager
            get() {
                return if (isRemote) {
                    clientSideInstance
                } else {
                    check(this is ServerWorld) {
                        "Server world is not a ServerWorld"
                    }
                    world.server.dataPackRegistries.gunPropertyManager
                }
            }

        val World.hasGunProperties: Boolean
            get() = !gunPropertyManager.properties.isNullOrEmpty()

        fun World.getGunProperties(item: Item): GunProperties {
            val itemId = checkNotNull(item.registryName) {
                "Cannot get GunProperties for unregistered item $item"
            }
            val properties = gunPropertyManager[itemId]
            check(properties != null) { "gun=$item is not a gun or has no gun properties" }

            return properties
        }

        val DataPackRegistries.gunPropertyManager: GunPropertyManager
            get() {
                return checkNotNull(map[this]) {
                    "Unregistered DataPackRegistries"
                }
            }
    }

}